﻿
SpringBoot内置tomcat，可以打包成jar包，通过主方法运行，本质上是独立的Java application，这种情况再放置于tomcat下运行，显然已不合适，当然，重新配置代码，并打成war包后仍可适用，这里我们只讲打成jar包的情况。在jar包的情况下，为此我查阅了网上资料。

在官网文档里，是有winsw的解决方案，链接如下：
[https://docs.spring.io/spring-boot/docs/1.5.9.RELEASE/reference/htmlsingle/#deployment-windows](https://docs.spring.io/spring-boot/docs/1.5.9.RELEASE/reference/htmlsingle/#deployment-windows) 
点击打开链接，除此之外，还有一种解决方案，那就是与tomcat使用相同的技术：procrun，
官方地址为：
[http://commons.apache.org/proper/commons-daemon/procrun.html](http://commons.apache.org/proper/commons-daemon/procrun.html)
点击打开链接。我们这里采用的是procrun的方式。

# 方法1：winsw

- https://docs.spring.io/spring-boot/docs/1.5.9.RELEASE/reference/htmlsingle/#deployment-windows

- https://github.com/winsw/winsw

### 下载

- v3.0.0-alpha.10

[WinSW-net461.exe](https://github.com/winsw/winsw/releases/download/v3.0.0-alpha.10/WinSW-net461.exe)

[WinSW-x64.exe](https://github.com/winsw/winsw/releases/download/v3.0.0-alpha.10/WinSW-x64.exe)

[WinSW-x86.exe](https://github.com/winsw/winsw/releases/download/v3.0.0-alpha.10/WinSW-x86.exe)

- v3.0.0-alpha.9

[WinSW-net461.exe](https://github.com/winsw/winsw/releases/download/v3.0.0-alpha.9/WinSW-net461.exe)

[WinSW-x64.exe](https://github.com/winsw/winsw/releases/download/v3.0.0-alpha.9/WinSW-x64.exe)

[WinSW-x86.exe](https://github.com/winsw/winsw/releases/download/v3.0.0-alpha.9/WinSW-x86.exe)

![1](1.png)

![2](2.png)

![3](3.png)

![4](4.png)

- v2.11.0

[sample-allOptions.xml](https://github.com/winsw/winsw/releases/download/v2.11.0/sample-allOptions.xml)

[sample-minimal.xml](https://github.com/winsw/winsw/releases/download/v2.11.0/sample-minimal.xml)

[WinSW-x64.exe](https://github.com/winsw/winsw/releases/download/v2.11.0/WinSW-x64.exe)

[WinSW-x86.exe](https://github.com/winsw/winsw/releases/download/v2.11.0/WinSW-x86.exe)

[WinSW.NET2.exe](https://github.com/winsw/winsw/releases/download/v2.11.0/WinSW.NET2.exe)

[WinSW.NET4.exe](https://github.com/winsw/winsw/releases/download/v2.11.0/WinSW.NET4.exe)

[WinSW.NET461.exe](https://github.com/winsw/winsw/releases/download/v2.11.0/WinSW.NET461.exe)

# 方法2：procrun

- http://commons.apache.org/proper/commons-daemon/procrun.html

[原文链接](https://blog.csdn.net/weixin_40326608/article/details/100123178)
[原文链接](https://blog.csdn.net/chemphone/article/details/79130108)
[原文链接](https://blog.csdn.net/bighuan/article/details/83416390)
